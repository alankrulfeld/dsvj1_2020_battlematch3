#pragma once


namespace match3
{
	
	enum class Scenes
	{
		mainMenu = 1, gameplay, credits, exit
	};
	extern Scenes scene;
	namespace mainmenu
	{
		void init( );
		void update( );
		void draw( );
		void deinit( );
	}
}




