#include "raylib.h"
#include "mainmenu.h"
namespace match3
{
	Texture2D background;
	const int tam = 5;
	const int amountMenu = 3;
	struct Button
	{
		Rectangle rec;
		Texture2D texture;
	};
	Button button[tam];
	namespace mainmenu
	{
		void init( )
		{
			button[static_cast< int >( Scenes::gameplay )].texture = LoadTexture("../res/texture/ui/PLAY.png");
			button[static_cast< int >( Scenes::credits )].texture = LoadTexture("../res/texture/ui/CREDITS.png");
			button[static_cast< int >( Scenes::exit )].texture = LoadTexture("../res/texture/ui/EXIT.png");

			for ( size_t i = 0; i < 5; i++ )
			{

				button[i].texture.height = 80;
				button[i].texture.width = 150;

			}

			background.height = GetScreenHeight( );
			background.width = GetScreenWidth( );
			background = LoadTexture("../res/texture/objects/BACKGROUNDMENU.png");
			for ( short i = 0; i < amountMenu; i++ )
			{
				button[i].rec.width = button[i].texture.width;
				button[i].rec.x = 400 - button[i].rec.width / 2;
				button[i].rec.height = button[i].texture.height;
			}
			button[0].rec.y = 200;
			button[1].rec.y = 300;

		}
		void update( )
		{
			for ( int i = 0; i < ( tam ); i++ )
			{
				if ( CheckCollisionPointRec(GetMousePosition( ), button[i].rec) )
				{
					if ( IsMouseButtonPressed(MOUSE_LEFT_BUTTON) )
					{
						switch ( i )
						{
						case 0:
							scene = Scenes::gameplay;
							break;




						case 1:
							CloseWindow( );
							break;



						default:

							break;
						}
					}
				}
			}
		}
		void draw( )
		{
			DrawTexture(background, 0, 0, WHITE);

			DrawText("Animals Match 3 by Alan Krulfeld", 200, 100, 30, BLACK);
			DrawTexture(button[static_cast< int >( Scenes::gameplay )].texture, 400 - button[static_cast< int >( Scenes::gameplay )].texture.width / 2, 200, WHITE);
			DrawTexture(button[static_cast< int >( Scenes::exit )].texture, 400 - button[static_cast< int >( Scenes::gameplay )].texture.width / 2, 300, WHITE);



		}
		void deinit( )
		{

		}
	}
}
