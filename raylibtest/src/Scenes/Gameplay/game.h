#include "raylib.h"

namespace match3
{

	namespace game
	{
		enum class BLOCKTYPE
		{
			CAT,
			OWL,
			RAINBOW,
			LIZARD,
			NONE
		};
		struct object
		{
			bool onSelection;
			bool active;
			Texture2D textureOff;
			Texture2D textureOn;
			BLOCKTYPE type;
			Rectangle rec;
		};
		void init( );
		void update( );
		void draw( );
		void deinit( );
	}
}
