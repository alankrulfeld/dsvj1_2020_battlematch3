#include "raylib.h"
#include "game.h"
#include <vector>
const int tam = 6;
using namespace std;
namespace match3
{
	bool gameover = false;

	namespace game
	{
		int combos=30;
		
		object objects[tam][tam];
		int score;
		BLOCKTYPE selectedType = BLOCKTYPE::NONE;
		vector<Vector2> selectPiecesPos;
		Texture2D background;

		void init( )
		{
			// Initialization
			//--------------------------------------------------------------------------------------
			const int screenWidth = 800;
			const int screenHeight = 500;
			int score = 0;

			InitWindow(screenWidth, screenHeight, "");

			background.height = GetScreenHeight( );
			background.width = GetScreenWidth( );
			background = LoadTexture("../res/texture/objects/BACKGROUND.png");


			SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
			//--------------------------------------------------------------------------------------
			for ( short i = 0; i < tam; i++ )
			{
				for ( short j = 0; j < tam; j++ )
				{
					int aux = GetRandomValue(0, 3);
					objects[i][j].active=true;
					switch ( static_cast< BLOCKTYPE >( aux ) )
					{

					case BLOCKTYPE::CAT:
						objects[i][j].type = BLOCKTYPE::CAT;
						objects[i][j].textureOff = LoadTexture("../res/texture/objects/Cat_sleep.png");
						objects[i][j].textureOn = LoadTexture("../res/texture/objects/Cat_wake up.png");
						
						break;
					case BLOCKTYPE::OWL:
						objects[i][j].type = BLOCKTYPE::OWL;
						objects[i][j].textureOff = LoadTexture("../res/texture/objects/owl_sleep.png");
						objects[i][j].textureOn = LoadTexture("../res/texture/objects/owl_wakeUp.png");
						break;
					case BLOCKTYPE::RAINBOW:
						objects[i][j].type = BLOCKTYPE::RAINBOW;
						objects[i][j].textureOff = LoadTexture("../res/texture/objects/Rainbow_sleep.png");
						objects[i][j].textureOn = LoadTexture("../res/texture/objects/Rainbow_wake up.png");
						break;
					case BLOCKTYPE::LIZARD:
						objects[i][j].type = BLOCKTYPE::LIZARD;
						objects[i][j].textureOff = LoadTexture("../res/texture/objects/Lizard_sleep.png");
						objects[i][j].textureOn = LoadTexture("../res/texture/objects/Lizard_wake up.png");
						break;

					default:
						objects[i][j].type = BLOCKTYPE::NONE;
						break;
					}


				}

			}
			for ( short i = 0; i < tam; i++ )
			{
				for ( short j = 0; j < tam; j++ )
				{

					objects[i][j].rec.width = 40;
					objects[i][j].rec.height = 40;
					objects[i][j].textureOff.height = objects[i][j].rec.height;
					objects[i][j].textureOff.width = objects[i][j].rec.width;
					objects[i][j].textureOn.height = objects[i][j].rec.height;
					objects[i][j].textureOn.width = objects[i][j].rec.width;

					objects[i][j].rec.x = 0.0f;
					objects[i][j].rec.y = 0.0f;
				}

			}

			int aX = 0;
			int aY = 100;
			for ( short i = 0; i < tam; i++ )
			{
				aX = 240;
				for ( short j = 0; j < tam; j++ )
				{
					objects[i][j].rec.x = aX;
					objects[i][j].rec.y = aY;
					aX += 50;

				}
				aY += 60;
			}

		}

		void update( )
		{
			
			if ( IsMouseButtonDown(MOUSE_LEFT_BUTTON) )
			{
				for ( short i = 0; i < tam; i++ )
				{
					for ( short j = 0; j < tam; j++ )
					{
						if ( CheckCollisionPointRec(GetMousePosition( ), objects[i][j].rec) )
						{
							if (  selectedType == BLOCKTYPE::NONE && !objects[i][j].onSelection )
							{
								objects[i][j].onSelection = true;
								selectPiecesPos.push_back({ static_cast< float >( i ), static_cast< float >( j ) });
								selectedType = objects[i][j].type;

							}
							else if( selectedType == objects[i][j].type && !objects[i][j].onSelection )
							{
								for ( short n = -1; n <= 1; n++ )
								{
									for ( short m = -1; m <= 1; m++ )
									{
										if ( i == selectPiecesPos.back( ).x + n && j == selectPiecesPos.back( ).y + m )
										{
											objects[i][j].onSelection = true;
											selectPiecesPos.push_back({ static_cast< float >( i ), static_cast< float >( j ) });
											selectedType = objects[i][j].type;
										}
									}

								}
							}
							if ( selectPiecesPos.size( ) > 0 )
							{

								if ( selectPiecesPos.back( ).x == i && selectPiecesPos.back( ).y == j )
								{
									return;
								}

								for ( int k = 0; k < selectPiecesPos.size( ); k++ )
								{
									if ( selectPiecesPos[k].x == i && selectPiecesPos[k].y == j )
									{
										do
										{
											objects[static_cast< int >( selectPiecesPos.back( ).x )][static_cast< int >( selectPiecesPos.back( ).y )].onSelection = false;
											selectPiecesPos.pop_back( );
										} while ( selectPiecesPos.back( ).x != i && selectPiecesPos.back( ).y != j );
									}
								}
							}
						}
					}
				}
			}
			else if ( IsMouseButtonReleased(MOUSE_LEFT_BUTTON) )
			{
				if ( selectPiecesPos.size()>2 )
				{
					score += selectPiecesPos.size( )*5;//regular
					combos--;
					for ( short i = 0; i < selectPiecesPos.size(); i++ )
					{
						int aux = GetRandomValue(0, 3);
						objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].active = false;

						switch ( static_cast< BLOCKTYPE >( aux ) )
						{

						case BLOCKTYPE::CAT:
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].type = BLOCKTYPE::CAT;
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOff = LoadTexture("../res/texture/objects/Cat_sleep.png");
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOn = LoadTexture("../res/texture/objects/Cat_wake up.png");
							break;
						case BLOCKTYPE::OWL:
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].type = BLOCKTYPE::OWL;
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOff = LoadTexture("../res/texture/objects/owl_sleep.png");
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOn = LoadTexture("../res/texture/objects/owl_wakeUp.png");
							break;
						case BLOCKTYPE::RAINBOW:
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].type = BLOCKTYPE::RAINBOW;
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOff = LoadTexture("../res/texture/objects/Rainbow_sleep.png");
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOn = LoadTexture("../res/texture/objects/Rainbow_wake up.png");
							break;
						case BLOCKTYPE::LIZARD:
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].type = BLOCKTYPE::LIZARD;
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOff = LoadTexture("../res/texture/objects/Lizard_sleep.png");
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOn = LoadTexture("../res/texture/objects/Lizard_wake up.png");
							break;

						default:
							objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].type = BLOCKTYPE::NONE;
							break;
						}
						objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].rec.width = 40;
						objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].rec.height = 40;
						objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOff.height = objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].rec.height;
						objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOff.width = objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].rec.width;
						objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOn.height = objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].rec.height;
						objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].textureOn.width = objects[static_cast< int >( selectPiecesPos[i].x )][static_cast< int >( selectPiecesPos[i].y )].rec.width;
					}
				}
				for ( short i = 0; i < tam; i++ )
				{
					for ( short j = 0; j < tam; j++ )
					{
						objects[i][j].onSelection = false;
						selectedType = BLOCKTYPE::NONE;
					}
				}
				selectPiecesPos.clear( );

			}



		}



		void draw( )
		{

			
			if ( combos == 0 )
			{
				DrawText(TextFormat("YOUR SCORE IS %4i ", score), 280, 200, 30, BLACK);
			}
			else
			{

				DrawTexture(background, 0, 0, WHITE);
				
				for ( short i = 0; i < tam; i++ )
				{
					for ( short j = 0; j < tam; j++ )
					{
						if ( objects[i][j].onSelection )
						{
							DrawTexture(objects[i][j].textureOn, static_cast< float >( objects[i][j].rec.x ), static_cast< float >( objects[i][j].rec.y ), WHITE);
						}
						else
						{
							DrawTexture(objects[i][j].textureOff, static_cast< float >( objects[i][j].rec.x ), static_cast< float >( objects[i][j].rec.y ), WHITE);
						}
					}
				}




				DrawText(TextFormat("SCORE: %4i ", score), 120, 20, 20, BLACK);
				DrawText(TextFormat("COMBOS:  %4i ", combos), 480, 20, 20, BLACK);
			}


		}

		//----------------------------------------------------------------------------------

		void deinit( )
		{

			CloseWindow( );        // Close window and OpenGL context
			//--------------------------------------------------------------------------------------

		}
		/*void creationObjects( )
		{
			int falseObjects=0;
			for ( int i = 5; i >= 0; i-- )
			{
				for ( int j = 5; j >= 0; j-- )
				{
					if ( objects[i][j].active==false )
					{
						falseObjects++;
					}
					else
					{
						objects[i][j];
					}
				}
			}
		}*/

	}

}
