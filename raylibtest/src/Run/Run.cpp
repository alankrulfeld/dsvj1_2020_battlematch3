#include "Scenes/Gameplay/mainmenu.h"
#include "Scenes/Gameplay/game.h"

#include "raylib.h"

	namespace match3
	{
		bool endGame = false;


		Scenes scene = Scenes::mainMenu;

		

		void init( )
		{
			const int screenWidth = 800;
			const int screenHeight = 500;
			SetTargetFPS(60);
			SetExitKey(KEY_F4);
		
			game::init( );
			mainmenu::init( );



		}

		void update( ) {

			switch ( scene )
			{
			case Scenes::mainMenu:
				mainmenu::update( );
				break;

			case Scenes::gameplay:
				game::update( );
				break;

				/*	case instructions:
						instructions::update( );
						break;*/

			default:
				break;
			}
		}

		void draw( ) 
		{

			BeginDrawing( );

			ClearBackground(WHITE);

			switch ( scene )
			{
			case Scenes::mainMenu:
				mainmenu::draw( );
				break;

			case Scenes::gameplay:
				game::draw( );
				break;

				//case instructions:
				//	instructions::draw( );
				//	break;

			default:
				break;
			}

			EndDrawing( );
		}

		void deinit( ) 
		{

			mainmenu::deinit( );
			game::deinit( );
		}
		void runGame( )
		{

			init( );

			while ( !WindowShouldClose( ) && !endGame )
			{
				update( );
				draw( );
			}

			deinit( );
		}
	}

